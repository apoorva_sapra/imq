from dataclasses import dataclass
from Protocol.ProrocolConstants import *

@dataclass
class ImqProtocol:
    data: str
    sourceUrl: str
    desitantionUrl: str
    dataformat: str = FORMAT
    version: str = VERSION
