from setuptools import find_packages
from setuptools import setup

setup(
    name='server',
    version='1.0.0',
    description='This package contains files related to server',
    author='Apoorva',
    author_email='apoorva.sapra@intimetec.com',
    packages=find_packages()
)