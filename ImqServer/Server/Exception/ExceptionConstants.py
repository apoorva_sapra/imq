SOCKET_EXCEPTION_MESSAGE='Socket could not be created'
DEFAULT_EXCEPTION_MESSAGE= "Something went wrong"
SERVER_LISTENER_EXCEPTION_MESSAGE="Server not listening for clients."
SERVER_ACCEPTOR_EXCEPTION_MESSAGE="Server not able to accept clients"